# Tapas（原 Tapastic）漫畫下載程式
# 網頁參考日期：2018 / 08 / 17 ，官方網站可能會更新網頁結構，屈時要更新截取程式。
# 作者：Dispy(Translate)
# 直譯環境：Python 3.6.5
# 僅供學習交流用。
import requests
from bs4 import BeautifulSoup as soup
from function import file_handler as handler
from function import text_writer as writer

def analyze(result, article_num):
    '''分析網頁，尋找漫畫來源。並顯示相關資訊。'''
    doc = soup(result.text, 'html.parser')

    series_title = ''
    episode_title = ''

    # 讀取作品名稱
    series_title_temp = doc.find_all(attrs={"class": "title js-series-btn"})
    for temp in series_title_temp:
        print('作品名稱：%s' % temp.string)
        series_title = temp.string
        writer.recode('作品名稱：%s' % temp.string)
        break

    # 讀取章節名稱
    title = doc.find_all(attrs={"class": "title"})
    for title_sub in title:
        print('章節名稱：%s' % title_sub.string)
        episode_title = title_sub.string
        writer.recode('章節名稱：%s' % title_sub.string)
        break

    folder = handler.check_folder('tapas', series_title, episode_title)
    writer.write_file(folder)

    # 讀取漫畫圖片連結
    links = [element.get('data-src') for element in doc.find_all(attrs={"class": "content__img js-lazy"})]
    for num, link in enumerate(links, start=1):
        format_num = "%02d" % num
        filename = '%s - %s %s.jpg' % (series_title, episode_title, str(format_num))
        handler.download_file(link, folder, filename)

# 因 Tapas 網站採用 https 協定，需要加入請求標頭，模擬瀏覽器瀏覽網頁。
# 不要加入 host 元素就正常了......
headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:61.0) Gecko/20100101 Firefox/61.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Language': 'zh-TW,zh;q=0.8,en-US;q=0.5,en;q=0.3',
    'Accept-Encoding': 'gzip, deflate, br',
    'DNT': '1',
    'Connection': 'keep-alive'
}

# 主程式
article_num = input('請輸入 Tapas 文章編號：') # 等候使用者輸入文章編號
target_url = 'https://tapas.io/episode/' + str(article_num)
print('讀取文章： %s' % target_url)
writer.recode('原始出處：%s' % target_url)
result = requests.get(target_url, verify=True, headers=headers)
if (result.status_code == 200):
    analyze(result, article_num)
else:
    print('連網時發生錯誤，錯誤代碼是：%s' % result.status_code)
