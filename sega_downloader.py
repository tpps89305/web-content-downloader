#資料夾的設置，就設定成 Sega/縮圖、Sega/大圖。
import requests
from bs4 import BeautifulSoup as soup
from function import file_handler as handler
from function import text_writer as writer
import threading

total_result = 0
pic_folder = handler.check_folder('SEGA', 'SEGA60th', 'pic')
pic_s_folder = handler.check_folder('SEGA', 'SEGA60th', 'pic_s')
other_folder = handler.check_folder('SEGA', 'SEGA60th', 'other')

def analyze(result):
    doc = soup(result.text, 'html.parser')

    links = [element.get('href') for element in doc.find_all(attrs={"target": "_blank"})]
    semaphore = threading.Semaphore(20)
    threads = []

    index = 0
    while index + 50 < len(links):
        new_index = index + 50
        new_thread = threading.Thread(target = parse_link, args=(doc, links[index:new_index], semaphore))
        new_thread.start()
        threads.append(new_thread)
        index = new_index + 1 #定位下一段資料的起點
    else:
        new_thread = threading.Thread(target = parse_link, args=(doc, links[index:len(links) - 1], semaphore))
        new_thread.start()
        threads.append(new_thread)
    
    for i in range(len(threads)):
        threads[i].join()

def parse_link(doc, links, semaphore):
    semaphore.acquire()
    for link in links:
        if str(link).endswith('.png'):
            # print(link)
            link_s = str(link).replace('.png', '_s.jpg')
            names = [element.get('alt') for element in doc.find_all(attrs={"src": link_s})]
            for name in names:
                # print(name)
                wish_data_map = {
                    'name': str(link).replace('/', '').replace('.png', '') + '_' + name.replace('/', ' '),
                    'pic': result.url + link,
                    'pic_s': result.url + link_s
                }
                # print(wish_data_map)
                handler.download_file(wish_data_map['pic'], pic_folder, wish_data_map['name'] + '.png')
                handler.download_file(wish_data_map['pic_s'], pic_s_folder, wish_data_map['name'] + '.jpg')
                global total_result
                total_result += 1
                writer.recode('%s\n%s\n%s\n' % (wish_data_map['name'], wish_data_map['pic'], wish_data_map['pic_s']))
    semaphore.release()

print("開始動作⋯⋯")
target_url = "https://sega.jp/special/tw60th/"
#不加請求檔頭就正常了⋯⋯
result = requests.get(target_url)
print(result.url)
analyze(result)
writer.write_file(other_folder)
print("執行完畢，得到了 " + str(total_result) + " 張圖片！")