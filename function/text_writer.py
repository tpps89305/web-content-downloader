'''文字檔寫入程式'''
data = ''

def recode(text):
    '''記錄文字到暫存區（沒有寫入到檔案）'''
    new_data = text + '\n'
    global data
    data += new_data

def write_file(folder_path):
    '''將之前暫存的文字，寫入到檔案，並存到 folder_path 裡。檔名為 detail.txt。'''
    global data
    fout = open(folder_path + '/_detail.txt', 'wt')
    print(fout.write(data))
    data = '' # 清除暫存區
