import os
import urllib3

def check_folder(folder_name, series_title, episode_title):
    '''準備儲存漫畫用的資料夾'''
    if(not os.path.exists('../%s' % folder_name)):
        os.mkdir('../%s' % folder_name)
    if(not os.path.exists('../%s/%s' % (folder_name, series_title))):
        os.mkdir('../%s/%s' % (folder_name, series_title))
    if(not os.path.exists('../%s/%s/%s' % (folder_name, series_title, episode_title))):
        os.mkdir('../%s/%s/%s' % (folder_name, series_title, episode_title))
    return '../%s/%s/%s' % (folder_name, series_title, episode_title)

def download_file(url, folder_path, filename):
    '''下載圖片到指定的資料夾'''
    http = urllib3.PoolManager()
    r = http.request('GET', url)
    if (r.status):
        try:
            with open('%s/%s' % (folder_path, filename), 'xb') as f:
                f.write(r.data)
            print('%s 下載完畢' % filename)
        except FileExistsError:
            print('%s 已存在，取消下載此檔案。' % filename)
    else:
        print('下載時發生錯誤！')
